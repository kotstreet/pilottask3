﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

/// <summary>
/// Class for work with *.csv file
/// </summary>
public static class WorkWithCSV
{
    /// <summary>
    /// check for file existence
    /// </summary>
    /// <returns>true if file exitence</returns>
    private static bool IsFileExists(string filePath)
    {
        if (File.Exists(filePath) == false)
        {
            MessageBox.Show(Constants.FileExistesErrorText, Constants.FileErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);

            return false;
        } else
        {
            return true;
        }
    }

    /// <summary>
    /// Read data from .csv file
    /// </summary>
    /// <returns>list of people from file or null when file is not existence</returns>
    public  static IEnumerable<Person> Read(string filePath) 
    {
        //check for file existence
        if (IsFileExists(filePath) == false)
        {
            return null;
        }

        List<Person> people = new List<Person>();

        using (TextFieldParser textFieldParser = new TextFieldParser(filePath))
        {
            //setting textFileReader up 
            textFieldParser.TextFieldType = FieldType.Delimited;
            textFieldParser.SetDelimiters(Constants.SeparatorForFile);

            //to know which line is current
            int lineNumber = 1;

            try
            {
                //read data and building list
                while (!textFieldParser.EndOfData)
                {
                    string[] lineFromFile = textFieldParser.ReadFields();
                    Person person = new Person(lineFromFile);
                    people.Add(person);
                    lineNumber++;
                }
            }
            catch (FormatException)
            {
                MessageBox.Show($"{Constants.ReadLineFromFileErrorText}{Environment.NewLine}{Constants.EncorrectLineFromFileText}{lineNumber}",
                    Constants.FileErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        return people;
    }

    /// <summary>
    /// Write data from list of people to .csv file 
    /// </summary>
    /// <param name="people">list of people for writing to the file</param>
    public async static void WriteAsync(List<Person> people, string filePath)
    {
        await Task.Factory.StartNew(() =>
        {
            //check for file existence
            if (IsFileExists(filePath) == false)
            {
                return;
            }

            StringBuilder lineForWrite = new StringBuilder();
            //write data to .csv file
            using (StreamWriter streamWriter = new StreamWriter(filePath))
            {
                foreach (Person person in people)
                {
                    //create string for write into .csv file
                    lineForWrite.Append(person.Date.ToShortDateString());
                    lineForWrite.Append(Constants.SeparatorForFile);
                    lineForWrite.Append(person.FirstName);
                    lineForWrite.Append(Constants.SeparatorForFile);
                    lineForWrite.Append(person.LastName);
                    lineForWrite.Append(Constants.SeparatorForFile);

                    lineForWrite.Append(person.SurName);
                    lineForWrite.Append(Constants.SeparatorForFile);
                    lineForWrite.Append(person.City);
                    lineForWrite.Append(Constants.SeparatorForFile);
                    lineForWrite.Append(person.Country);

                    //write line into .csv file
                    streamWriter.WriteLine(lineForWrite.ToString());

                    //Reset lineForWrite for write new line from zero
                    lineForWrite.Clear();
   
                }
            }
        });
    }
}