﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Constants
{
    //Setting open file dialog
    public const string ChoiceFileForWrite = "Выберете файл для записи";
    public const string ChoiceFileForRead = "Выберете файл для чтения";
    public const string ChoiceFileForSelect = "Выберете файл для выборки данных";
    public const string FilterForFiles = "comma separated values files (*.csv)|*.csv|All files (*.*)|*.*";
    public const string FilterForExcelFiles = "Microsoft Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";

    //WorkWithCSV
    public const string FileErrorCaption = "Ошибка";
    public const string FileExistesErrorText = "Возникла ошибка, проверьте сущществование указанного файла в дирректории";
    public const string ReadLineFromFileErrorText = "Возникла ошибка при чтении из файла.";
    public const string EncorrectLineFromFileText = "Ошибка возникла в строке номер ";

    /// <summary>
    /// separator for data to .csv file
    /// </summary>
    public const string SeparatorForFile = ";";

    public const string InformationAboutEndProccess = "Информация о выполнении действия";
       
    //form1
    public const string CaptionFormWhenDataProccess = "Идет выполнение процесса, пожалуйста, ожидайте.";

    public const string CaptionRequestReloadData = "Подтвердите действие";
    public const string TextRequesReloadData = "Вы уверены, что хотите обновить данные?";
    public const string TextWarningReloadData = "Это может занять некоторое время.";
    public const string TextNotificationReloadDataForm = "Удаление старых данных и загрузка новых завершены.";
    public const string TextNotificationLoadDataForm = "Загрузка новых завершены.";

    public const string CaptionClosingForm = "Подтвердите действие";
    public const string TextClosingForm = "Вы действительно хотите отменить выборку данных?";
    public const string TextClosingMainForm = "Вы действительно хотите выйти из приложения?";

    public const string TextNotificationWriteDataToForm = "Запись данных в файл завершена успешно.";

    public const string GetIdError = "Ошибка получения id для изменения данных";
    public const string NotificationSingleDelete = "Объект удален";
    public const string NotificationErrorDelete = "Выберете одну запись для удаления!";

    public const string RequestConvermationCaption = "Подтверждение действия";
    public const string RequestConvermationText = "Вы действительно хотите удалить выбранный элемент?";

    public const int SingleSelecteValue = 1;

    public const int AddOneDay = 1;
    public const int SubtractionOneDay = -1;
    public const int CompareForEqualsValue = 0;


    public const string NotificationRefresh = "Информация о человеке обновлена";
    public const string CaptionChangeData = "Изменение данных";
    public const string NotificationErrorChange = "Выберете одну запись для ее изменения!";

    public const string NotificationAdd = "Человек добавлен в список";
    public const string CaptionAddData = "Добавление данных";

    public const string TextMainDataMultiply = "Начиная с";
    public const string TextMainDataSingle = "Дата";

    //for work with excel
    public const string WaitBeforeFillExcel = "Данные будут передаваться в эксель в фоновом режиме, вы можете продолжать работу в приложении, ексель с данными откроется автоматически, псле завершения экспорта";

    public const int WidthForColumns = 20;
    public const int IncreaceForFontSize = 5;
    public const int AddingIndexForToStartFillingData = 2;
    
    public const int RowForHeaderInExcel = 1;

    public const string HeaderTextForSecondName = "Фамилия";
    public const string HeaderTextForFirstName = "Имя";
    public const string HeaderTextForSurName = "Отчество";
    public const string HeaderTextForCity = "Город";
    public const string HeaderTextForCountry = "Страна";
    public const string HeaderTextForDate = "Дата";

}