﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

namespace PilotTask3
{
    public partial class frmAllData : Form
    {
        /// <summary>
        /// prepares the database for the main work
        /// </summary>
        public frmAllData()
        {
            InitializeComponent();

            using (PersonContext db = new PersonContext())
            {
                //Check for empty in database and load data
                int peopleCount = db.People.Count();

                if (peopleCount > 0)
                {
                    db.People.Load();
                }

                dgvPeople.DataSource = db.People.ToList();
            }   
        }

        /// <summary>
        /// add new data
        /// </summary>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //Create form for add data
            frmData frmData = new frmData();
            frmData.Text = Constants.CaptionAddData;

            DialogResult resault = frmData.ShowDialog(this);

            if (resault == DialogResult.Cancel)
                return;

            //Create new person with data from dialog form
            Person person = new Person();
            person.Date = frmData.dtpDate.Value;
            person.FirstName = frmData.txtFirstName.Text;
            person.LastName = frmData.txtLastName.Text;
            person.SurName = frmData.txtSurName.Text;
            person.City = frmData.txtCity.Text;
            person.Country = frmData.txtCountry.Text;

            //save data to database
            using (PersonContext db = new PersonContext())
            {
                db.People.Add(person);
                db.SaveChanges();
                dgvPeople.DataSource = db.People.ToList();
            }
                
            MessageBox.Show(Constants.NotificationAdd, Constants.InformationAboutEndProccess, MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        /// <summary>
        /// change data
        /// </summary>
        private void btnEdit_Click(object sender, EventArgs e)
        {

            //change data 
            if (dgvPeople.SelectedRows.Count == Constants.SingleSelecteValue)
            {
                //get id the first person from dataGridView
                int index = dgvPeople.SelectedRows[0].Index;
                int id = 0;
                bool converted = Int32.TryParse(dgvPeople[0, index].Value.ToString(), out id);
                if (converted == false)
                {
                    MessageBox.Show(Constants.GetIdError);
                    return;
                }

                //get person by id
                Person person;
                using (PersonContext db = new PersonContext())
                {
                    person = db.People.Find(id);

                    //create form for change data
                    frmData frmData = new frmData();
                    frmData.Text = Constants.CaptionChangeData;

                    //entering data to fields
                    frmData.dtpDate.Value = person.Date;
                    frmData.txtFirstName.Text = person.FirstName;
                    frmData.txtLastName.Text = person.LastName;
                    frmData.txtSurName.Text = person.SurName;
                    frmData.txtCity.Text = person.City;
                    frmData.txtCountry.Text = person.Country;

                    DialogResult result = frmData.ShowDialog(this);

                    if (result == DialogResult.Cancel)
                        return;

                    //save data from filelds
                    person.Date = frmData.dtpDate.Value;
                    person.FirstName = frmData.txtFirstName.Text;
                    person.LastName = frmData.txtLastName.Text;
                    person.SurName = frmData.txtSurName.Text;
                    person.City = frmData.txtCity.Text;
                    person.Country = frmData.txtCountry.Text;

                    //save data into database and notificate about it
                    db.SaveChanges();
                    dgvPeople.DataSource = db.People.ToList();
                }

                MessageBox.Show(Constants.NotificationRefresh);

            }
            //notification about neccessary to select a row for reset information
            else
            {
                MessageBox.Show(Constants.NotificationErrorChange, Constants.InformationAboutEndProccess, MessageBoxButtons.OK, 
                    MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// delete data
        /// </summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            int selectRowsCount = dgvPeople.SelectedRows.Count;
            if (selectRowsCount == Constants.SingleSelecteValue)
            {
                //request for convermation of deletion
                DialogResult resultRemoveRequest = MessageBox.Show(
                    Constants.RequestConvermationText,
                    Constants.RequestConvermationCaption, 
                    MessageBoxButtons.YesNo);

                if (resultRemoveRequest == DialogResult.No)
                {
                    return;
                }

                int idColumn = 0;

                //get id the person from dataGridView
                int index = dgvPeople.Rows.IndexOf(dgvPeople.SelectedRows[0]);
                bool converted = Int32.TryParse(dgvPeople[idColumn, index].Value.ToString(), out int id);
                if (converted == false)
                {
                    MessageBox.Show(Constants.GetIdError);
                    return;
                }

                using (PersonContext db = new PersonContext())
                {
                    //get person by id
                    Person person = db.People.Find(id);

                    db.People.Remove(person);

                    //save changes with data to database
                    db.SaveChanges();
                    dgvPeople.DataSource = db.People.ToList();
                }

                //notification about remove
                MessageBox.Show(Constants.NotificationSingleDelete, Constants.InformationAboutEndProccess, MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            } 
            else
            //notification about neccessary to select some row for remove
            {
                MessageBox.Show(Constants.NotificationErrorDelete);
            }
        }

        /// <summary>
        /// write data from db to .csv file
        /// </summary>
        private void btnWrite_Click(object sender, EventArgs e)
        {
            //get data for write
            List<Person> people;
            using (PersonContext db = new PersonContext())
            {
                people = db.People.ToList<Person>();
            }

            //create and setting open file dialog
            var ofd = new OpenFileDialog();
            ofd.Title = Constants.ChoiceFileForWrite;
            ofd.Filter = Constants.FilterForFiles;
            var inithialPath = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).FullName).FullName;
            ofd.InitialDirectory = inithialPath;

            //show dialog and get selected file
            var result = ofd.ShowDialog();
            if (result == DialogResult.Cancel)
            {
                return ;
            }
            var filePath = ofd.FileName;

            //write data
            WorkWithCSV.WriteAsync(people, filePath);

            //notification about successe write to file
            MessageBox.Show(Constants.TextNotificationWriteDataToForm, Constants.InformationAboutEndProccess, MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        /// <summary>
        /// Close the form
        /// </summary>
        private void frmAllData_FormClosing(object sender, FormClosingEventArgs e)
        {
            //request for exit
            DialogResult result = MessageBox.Show(Constants.TextClosingMainForm, Constants.CaptionClosingForm, MessageBoxButtons.YesNo);

            if(result == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// delete data from database and read them from file
        /// </summary>
        private async void btnRead_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show($"{Constants.TextRequesReloadData}{Environment.NewLine}{Constants.TextWarningReloadData}", 
                Constants.CaptionRequestReloadData, MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes) 
            {
                DisableUI();

                //notice about proccessing
                var form = ((sender as Button).Parent as Form);
                var caption = TextFormAsNotice(form);

                using (PersonContext db = new PersonContext())
                {
                    await ResetDataAsync(db);

                    //////////////////////////////////////////////////////////////////////////////////////////
                    //load data 
                    await db.People.LoadAsync();

                    dgvPeople.DataSource = db.People.ToList();
                }

                TextFormAsNoticeCancel(form, caption);

                AbleUI();
            }
        }

        /// <summary>
        /// remove old data from PersonContext and add new data from csv file
        /// </summary>
        private async Task<bool> ResetDataAsync(PersonContext db, bool isRewrite = true)
        {
            //create and setting open file dialog
            var ofd = new OpenFileDialog();
            ofd.Title = Constants.ChoiceFileForRead;
            ofd.Filter = Constants.FilterForFiles;
            var inithialPath = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).FullName).FullName;
            ofd.InitialDirectory = inithialPath;

            //show dialog and get selected file
            var result = ofd.ShowDialog();
            if (result == DialogResult.Cancel)
            {
                return false;
            }
            var filePath = ofd.FileName;

            //read data, add to db and fill show them
            await Task.Factory.StartNew(() =>
            {
                //people from .csv file
                var people = WorkWithCSV.Read(filePath);

                if (people != null)
                {
                    //remove old data
                    if(isRewrite)
                    {
                        db.People.RemoveRange(db.People);
                    }

                    //add new data
                    people.ToList().ForEach(person => 
                    { 
                        db.People.Add(person);
                    });
                    db.SaveChanges();

                    //info about end of the proccess
                    if(isRewrite)
                    {
                        MessageBox.Show(Constants.TextNotificationReloadDataForm, Constants.InformationAboutEndProccess, MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show(Constants.TextNotificationLoadDataForm, Constants.InformationAboutEndProccess, MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                }
            });

            return true;
        }

        /// <summary>
        /// Open another form for select data
        /// </summary>
        private void btnSelectData_Click(object sender, EventArgs e)
        {
            //create form for selection data and focus to it
            frmDataSelection frmDataSelection = new frmDataSelection();
            frmDataSelection.Show(this);
        }

        /// <summary>
        /// Add data from .csv into database
        /// </summary>
        private async void btnRead_Click_1(object sender, EventArgs e)
        {
            var result = MessageBox.Show($"{Constants.TextRequesReloadData}{Environment.NewLine}{Constants.TextWarningReloadData}",
                Constants.CaptionRequestReloadData, MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                DisableUI();

                //notice about proccessing
                var form = ((sender as Button).Parent as Form);
                var caption = TextFormAsNotice(form);
                
                //work with data
                using (PersonContext db = new PersonContext())
                {
                    await ResetDataAsync(db, false);

                    //load data 
                    await db.People.LoadAsync();
                    dgvPeople.DataSource = db.People.ToList();

                }

                //reset text of the form
                TextFormAsNoticeCancel(form, caption);

                AbleUI();
            }
        }

        /// <summary>
        /// set the all buttons in disabled state
        /// </summary>
        private void DisableUI()
        {
            btnAdd.Enabled = false;
            btnDelete.Enabled = false;
            btnEdit.Enabled = false;
            btnSelectData.Enabled = false;
            
            btnWrite.Enabled = false;
            btnRead.Enabled = false;
            btnReadAndClear.Enabled = false;
        }

        /// <summary>
        /// set the all buttons in enabled state
        /// </summary>
        private void AbleUI()
        {
            btnAdd.Enabled = true;
            btnDelete.Enabled = true;
            btnEdit.Enabled = true;
            btnSelectData.Enabled = true;

            btnWrite.Enabled = true;
            btnRead.Enabled = true;
            btnReadAndClear.Enabled = true;
        }

        /// <summary>
        /// set the notice in text of the form
        /// </summary>
        /// <returns>started caption of the form</returns>
        private string TextFormAsNotice(Form form)
        {
            var caption = form.Text;
            form.Text = Constants.CaptionFormWhenDataProccess;

            return caption;
        }

        /// <summary>
        /// reset text of the form
        /// </summary>
        /// <param name="caption">text of the form</param>
        private void TextFormAsNoticeCancel(Form form, string caption)
        {
            form.Text = caption;
        }
    }
}