﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PilotTask3
{
    public partial class frmDataSelection : Form
    {
        public frmDataSelection()
        {
            InitializeComponent();
        }

        private void ckhCountry_CheckedChanged(object sender, EventArgs e)
        {
            //change enable of field independ on checked checkBox
            if(ckhCountry.Checked)
            {
                txtCountry.Enabled = true;
            }
            else
            {
                txtCountry.Enabled = false;
            }
        }

        private void chkCity_CheckedChanged(object sender, EventArgs e)
        {
            //change enable of field independ on checked checkBox
            if (chkCity.Checked)
            {
                txtCity.Enabled = true;
            }
            else
            {
                txtCity.Enabled = false;
            }
        }

        private void chkSurName_CheckedChanged(object sender, EventArgs e)
        {
            //change enable of field independ on checked checkBox
            if (chkSurName.Checked)
            {
                txtSurName.Enabled = true;
            }
            else
            {
                txtSurName.Enabled = false;
            }
        }

        private void chkFirstName_CheckedChanged(object sender, EventArgs e)
        {
            //change enable of field independ on checked checkBox
            if (chkFirstName.Checked)
            {
                txtFirstName.Enabled = true;
            }
            else
            {
                txtFirstName.Enabled = false;
            }
        }

        private void chkSecondName_CheckedChanged(object sender, EventArgs e)
        {
            //change enable of field independ on checked checkBox
            if (chkSecondName.Checked)
            {
                txtSecondName.Enabled = true;
            }
            else
            {
                txtSecondName.Enabled = false;
            }
        }

        private void rboMultiDate_CheckedChanged(object sender, EventArgs e)
        {
            //show label and dataPicker for select interval of dates
            lblAdditionalDate.Show();
            dtpAdditionalDate.Show();
            lblMainDate.Text = Constants.TextMainDataMultiply;
        }

        private void rboSingleDate_CheckedChanged(object sender, EventArgs e)
        {
            //hide label and datePicker for select single date
            lblAdditionalDate.Hide();
            dtpAdditionalDate.Hide();
            lblMainDate.Text = Constants.TextMainDataSingle;
        }

        private void chkDate_CheckedChanged(object sender, EventArgs e)
        {
            //change enable of field independ on checked checkBox
            if (chkDate.Checked)
            {
                grpSelectionOfDate.Enabled = true;
            }
            else
            {
                grpSelectionOfDate.Enabled = false;
            }
        }

        private void frmDataSelection_FormClosing(object sender, FormClosingEventArgs e)
        {
            //request for exit
            DialogResult result = MessageBox.Show(Constants.TextClosingForm, Constants.CaptionClosingForm, MessageBoxButtons.YesNo);

            if (result == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //close form for select data
            btnCancel.FindForm().Close();
        }

        /// <summary>
        /// select data for write iinto file
        /// </summary>
        private async void btnSelect_Click(object sender, EventArgs e)
        {
            //create and setting open file dialog
            var ofd = new OpenFileDialog();
            ofd.Title = Constants.ChoiceFileForSelect;
            ofd.Filter = Constants.FilterForExcelFiles;
            var inithialPath = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).FullName).FullName;
            ofd.InitialDirectory = inithialPath;

            //show dialog and get selected file
            var result = ofd.ShowDialog();
            if (result == DialogResult.Cancel)
            {
                return;
            }
            var pathForFile = ofd.FileName;

            //Message abote fetching in the background
            MessageBox.Show(Constants.WaitBeforeFillExcel);

            //crete final result of the selection
            List<Person> people = new List<Person>();

            using (PersonContext db = new PersonContext())
            { 
                people = db.People.ToList();
            }

            //check for checked second name
            if (chkSecondName.Checked)
            {
                //get value from textbox
                string secondName = txtSecondName.Text.ToUpper();

                //select data that are suitable for the condition
                people = people.Where(p => p.LastName.ToUpper().Equals(secondName)).ToList();
            }

            //check for checked first name
            if(chkFirstName.Checked)
            {
                //get value from textbox
                string firstNAme = txtFirstName.Text.ToUpper();

                //select data that are suitable for the condition
                people = people.Where(p => p.FirstName.ToUpper().Equals(firstNAme)).ToList();
            }

            //check for checked surname
            if (chkSurName.Checked)
            {
                //get value from textbox
                string surName = txtSurName.Text.ToUpper();

                //select data that are suitable for the condition
                people = people.Where(p => p.SurName.ToUpper().Equals(surName)).ToList();
            }

            //check for checked city
            if (chkCity.Checked)
            {
                //get value from textbox
                string city = txtCity.Text.ToUpper();

                //select data that are suitable for the condition
                people = people.Where(p => p.City.ToUpper().Equals(city)).ToList();
            }

            //check for checked country
            if (ckhCountry.Checked)
            {
                //get value from textbox
                string country = txtCountry.Text.ToUpper();

                //select data that are suitable for the condition
                people = people.Where(p => p.Country.ToUpper().Equals(country)).ToList();
            }

            //check for checked date
            if (chkDate.Checked)
            {
                //check for select several date
                if(rboMultiDate.Checked)
                {
                    //get values from textboxes
                    DateTime startDate = dtpMainDate.Value;
                    DateTime finishDate = dtpAdditionalDate.Value;

                    //set date for choose(create right border), work with day and time
                    startDate = startDate.AddDays(Constants.SubtractionOneDay);
                    startDate = Convert.ToDateTime(startDate.ToShortDateString());
                    finishDate = finishDate.AddDays(Constants.AddOneDay);
                    finishDate = Convert.ToDateTime(finishDate.ToShortDateString());

                    //select data that are suitable for the condition
                    people = (from p in people
                              where startDate.CompareTo(p.Date) < Constants.CompareForEqualsValue && 
                                  finishDate.CompareTo(p.Date) > Constants.CompareForEqualsValue
                              select p).ToList();
                }
                else
                {
                    //get value from textbox 
                    string date = dtpMainDate.Value.ToShortDateString().ToUpper();

                    //select data that are suitable for the condition
                    people = people.Where(p => p.Date.ToShortDateString().ToUpper().Equals(date)).ToList();
                }
            }

            //prepare for selecting data
            this.btnSelect.Enabled = false;
            this.btnCancel.Enabled = false;

            //request and write to the excel file 
            WorkWithExcel workWithExcel = new WorkWithExcel();
            await Task.Run(() => workWithExcel.Execute(people, pathForFile));

            //prepare for continue work 
            this.btnSelect.Enabled = true;
            this.btnCancel.Enabled = true;
        }
    }
}
