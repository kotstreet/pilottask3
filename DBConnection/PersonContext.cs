﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

public class PersonContext : DbContext
{
    public PersonContext()
    {
        Database.SetInitializer<PersonContext>(new CreateDatabaseIfNotExists<PersonContext>());
    }

    public DbSet<Person> People { get; set; }

}
