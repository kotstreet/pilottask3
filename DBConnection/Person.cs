﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

public class Person
{
    public int Id { get; set; }
    public DateTime Date { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string SurName { get; set; }
    public string City { get; set; }
    public string Country { get; set; }

    /// <summary>
    /// Create person with data from lineFromFile
    /// </summary>
    /// <param name="lineFromFile"> filed from csv file</param>
    public Person(string[] lineFromFile)
    {
        //Create new person for add him to list
        Date = Convert.ToDateTime(lineFromFile[0]);
        FirstName = lineFromFile[1];
        LastName = lineFromFile[2];

        SurName = lineFromFile[3];
        City = lineFromFile[4];
        Country = lineFromFile[5];
    }

    /// <summary>
    /// Create person with default data
    /// </summary>
    public Person()
    {
        Date = DateTime.Now;
        FirstName = null;
        LastName = null;
        SurName = null;
        City = null;
        Country = null;
    }
}