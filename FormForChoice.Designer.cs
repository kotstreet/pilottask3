﻿namespace PilotTask3
{
    partial class frmDataSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpFieldForSelection = new System.Windows.Forms.GroupBox();
            this.chkDate = new System.Windows.Forms.CheckBox();
            this.ckhCountry = new System.Windows.Forms.CheckBox();
            this.chkCity = new System.Windows.Forms.CheckBox();
            this.chkSurName = new System.Windows.Forms.CheckBox();
            this.chkFirstName = new System.Windows.Forms.CheckBox();
            this.chkSecondName = new System.Windows.Forms.CheckBox();
            this.grpSelectionOfTextFields = new System.Windows.Forms.GroupBox();
            this.txtCountry = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.txtSurName = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtSecondName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.grpSelectionOfDate = new System.Windows.Forms.GroupBox();
            this.grpSelectionOfFormatData = new System.Windows.Forms.GroupBox();
            this.rboSingleDate = new System.Windows.Forms.RadioButton();
            this.rboMultiDate = new System.Windows.Forms.RadioButton();
            this.dtpAdditionalDate = new System.Windows.Forms.DateTimePicker();
            this.dtpMainDate = new System.Windows.Forms.DateTimePicker();
            this.lblAdditionalDate = new System.Windows.Forms.Label();
            this.lblMainDate = new System.Windows.Forms.Label();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.grpFieldForSelection.SuspendLayout();
            this.grpSelectionOfTextFields.SuspendLayout();
            this.grpSelectionOfDate.SuspendLayout();
            this.grpSelectionOfFormatData.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpFieldForSelection
            // 
            this.grpFieldForSelection.Controls.Add(this.chkDate);
            this.grpFieldForSelection.Controls.Add(this.ckhCountry);
            this.grpFieldForSelection.Controls.Add(this.chkCity);
            this.grpFieldForSelection.Controls.Add(this.chkSurName);
            this.grpFieldForSelection.Controls.Add(this.chkFirstName);
            this.grpFieldForSelection.Controls.Add(this.chkSecondName);
            this.grpFieldForSelection.Location = new System.Drawing.Point(22, 22);
            this.grpFieldForSelection.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.grpFieldForSelection.Name = "grpFieldForSelection";
            this.grpFieldForSelection.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.grpFieldForSelection.Size = new System.Drawing.Size(651, 86);
            this.grpFieldForSelection.TabIndex = 0;
            this.grpFieldForSelection.TabStop = false;
            this.grpFieldForSelection.Text = "Поля для выбора";
            // 
            // chkDate
            // 
            this.chkDate.AutoSize = true;
            this.chkDate.Location = new System.Drawing.Point(540, 35);
            this.chkDate.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.chkDate.Name = "chkDate";
            this.chkDate.Size = new System.Drawing.Size(73, 28);
            this.chkDate.TabIndex = 5;
            this.chkDate.Text = "Дата";
            this.chkDate.UseVisualStyleBackColor = true;
            this.chkDate.CheckedChanged += new System.EventHandler(this.chkDate_CheckedChanged);
            // 
            // ckhCountry
            // 
            this.ckhCountry.AutoSize = true;
            this.ckhCountry.Location = new System.Drawing.Point(436, 35);
            this.ckhCountry.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.ckhCountry.Name = "ckhCountry";
            this.ckhCountry.Size = new System.Drawing.Size(94, 28);
            this.ckhCountry.TabIndex = 4;
            this.ckhCountry.Text = "Страна";
            this.ckhCountry.UseVisualStyleBackColor = true;
            this.ckhCountry.CheckedChanged += new System.EventHandler(this.ckhCountry_CheckedChanged);
            // 
            // chkCity
            // 
            this.chkCity.AutoSize = true;
            this.chkCity.Location = new System.Drawing.Point(341, 34);
            this.chkCity.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.chkCity.Name = "chkCity";
            this.chkCity.Size = new System.Drawing.Size(85, 28);
            this.chkCity.TabIndex = 3;
            this.chkCity.Text = "Город";
            this.chkCity.UseVisualStyleBackColor = true;
            this.chkCity.CheckedChanged += new System.EventHandler(this.chkCity_CheckedChanged);
            // 
            // chkSurName
            // 
            this.chkSurName.AutoSize = true;
            this.chkSurName.Location = new System.Drawing.Point(214, 35);
            this.chkSurName.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.chkSurName.Name = "chkSurName";
            this.chkSurName.Size = new System.Drawing.Size(117, 28);
            this.chkSurName.TabIndex = 2;
            this.chkSurName.Text = "Отчество";
            this.chkSurName.UseVisualStyleBackColor = true;
            this.chkSurName.CheckedChanged += new System.EventHandler(this.chkSurName_CheckedChanged);
            // 
            // chkFirstName
            // 
            this.chkFirstName.AutoSize = true;
            this.chkFirstName.Location = new System.Drawing.Point(139, 35);
            this.chkFirstName.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.chkFirstName.Name = "chkFirstName";
            this.chkFirstName.Size = new System.Drawing.Size(65, 28);
            this.chkFirstName.TabIndex = 1;
            this.chkFirstName.Text = "Имя";
            this.chkFirstName.UseVisualStyleBackColor = true;
            this.chkFirstName.CheckedChanged += new System.EventHandler(this.chkFirstName_CheckedChanged);
            // 
            // chkSecondName
            // 
            this.chkSecondName.AutoSize = true;
            this.chkSecondName.Location = new System.Drawing.Point(19, 35);
            this.chkSecondName.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.chkSecondName.Name = "chkSecondName";
            this.chkSecondName.Size = new System.Drawing.Size(110, 28);
            this.chkSecondName.TabIndex = 0;
            this.chkSecondName.Text = "Фамилия";
            this.chkSecondName.UseVisualStyleBackColor = true;
            this.chkSecondName.CheckedChanged += new System.EventHandler(this.chkSecondName_CheckedChanged);
            // 
            // grpSelectionOfTextFields
            // 
            this.grpSelectionOfTextFields.Controls.Add(this.txtCountry);
            this.grpSelectionOfTextFields.Controls.Add(this.txtCity);
            this.grpSelectionOfTextFields.Controls.Add(this.txtSurName);
            this.grpSelectionOfTextFields.Controls.Add(this.txtFirstName);
            this.grpSelectionOfTextFields.Controls.Add(this.txtSecondName);
            this.grpSelectionOfTextFields.Controls.Add(this.label5);
            this.grpSelectionOfTextFields.Controls.Add(this.label4);
            this.grpSelectionOfTextFields.Controls.Add(this.label3);
            this.grpSelectionOfTextFields.Controls.Add(this.label2);
            this.grpSelectionOfTextFields.Controls.Add(this.label1);
            this.grpSelectionOfTextFields.Location = new System.Drawing.Point(22, 120);
            this.grpSelectionOfTextFields.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.grpSelectionOfTextFields.Name = "grpSelectionOfTextFields";
            this.grpSelectionOfTextFields.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.grpSelectionOfTextFields.Size = new System.Drawing.Size(447, 264);
            this.grpSelectionOfTextFields.TabIndex = 1;
            this.grpSelectionOfTextFields.TabStop = false;
            this.grpSelectionOfTextFields.Text = "Выбор текстовых полей";
            // 
            // txtCountry
            // 
            this.txtCountry.Enabled = false;
            this.txtCountry.Location = new System.Drawing.Point(186, 216);
            this.txtCountry.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(180, 29);
            this.txtCountry.TabIndex = 9;
            // 
            // txtCity
            // 
            this.txtCity.Enabled = false;
            this.txtCity.Location = new System.Drawing.Point(186, 175);
            this.txtCity.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(180, 29);
            this.txtCity.TabIndex = 8;
            // 
            // txtSurName
            // 
            this.txtSurName.Enabled = false;
            this.txtSurName.Location = new System.Drawing.Point(186, 134);
            this.txtSurName.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtSurName.Name = "txtSurName";
            this.txtSurName.Size = new System.Drawing.Size(180, 29);
            this.txtSurName.TabIndex = 7;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Enabled = false;
            this.txtFirstName.Location = new System.Drawing.Point(186, 93);
            this.txtFirstName.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(180, 29);
            this.txtFirstName.TabIndex = 6;
            // 
            // txtSecondName
            // 
            this.txtSecondName.Enabled = false;
            this.txtSecondName.Location = new System.Drawing.Point(186, 52);
            this.txtSecondName.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtSecondName.Name = "txtSecondName";
            this.txtSecondName.Size = new System.Drawing.Size(180, 29);
            this.txtSecondName.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 219);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 24);
            this.label5.TabIndex = 4;
            this.label5.Text = "Страна";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 178);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 24);
            this.label4.TabIndex = 3;
            this.label4.Text = "Город";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 137);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "Отчество";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 96);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Имя";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 55);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Фамилия";
            // 
            // grpSelectionOfDate
            // 
            this.grpSelectionOfDate.Controls.Add(this.grpSelectionOfFormatData);
            this.grpSelectionOfDate.Controls.Add(this.dtpAdditionalDate);
            this.grpSelectionOfDate.Controls.Add(this.dtpMainDate);
            this.grpSelectionOfDate.Controls.Add(this.lblAdditionalDate);
            this.grpSelectionOfDate.Controls.Add(this.lblMainDate);
            this.grpSelectionOfDate.Enabled = false;
            this.grpSelectionOfDate.Location = new System.Drawing.Point(490, 120);
            this.grpSelectionOfDate.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.grpSelectionOfDate.Name = "grpSelectionOfDate";
            this.grpSelectionOfDate.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.grpSelectionOfDate.Size = new System.Drawing.Size(370, 264);
            this.grpSelectionOfDate.TabIndex = 2;
            this.grpSelectionOfDate.TabStop = false;
            this.grpSelectionOfDate.Text = "Выбор даты";
            // 
            // grpSelectionOfFormatData
            // 
            this.grpSelectionOfFormatData.Controls.Add(this.rboSingleDate);
            this.grpSelectionOfFormatData.Controls.Add(this.rboMultiDate);
            this.grpSelectionOfFormatData.Location = new System.Drawing.Point(11, 35);
            this.grpSelectionOfFormatData.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.grpSelectionOfFormatData.Name = "grpSelectionOfFormatData";
            this.grpSelectionOfFormatData.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.grpSelectionOfFormatData.Size = new System.Drawing.Size(342, 114);
            this.grpSelectionOfFormatData.TabIndex = 8;
            this.grpSelectionOfFormatData.TabStop = false;
            this.grpSelectionOfFormatData.Text = "Формат выбора даты";
            // 
            // rboSingleDate
            // 
            this.rboSingleDate.AutoSize = true;
            this.rboSingleDate.Location = new System.Drawing.Point(10, 34);
            this.rboSingleDate.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.rboSingleDate.Name = "rboSingleDate";
            this.rboSingleDate.Size = new System.Drawing.Size(246, 28);
            this.rboSingleDate.TabIndex = 0;
            this.rboSingleDate.Text = "Выбор конкретной даты";
            this.rboSingleDate.UseVisualStyleBackColor = true;
            this.rboSingleDate.CheckedChanged += new System.EventHandler(this.rboSingleDate_CheckedChanged);
            // 
            // rboMultiDate
            // 
            this.rboMultiDate.AutoSize = true;
            this.rboMultiDate.Checked = true;
            this.rboMultiDate.Location = new System.Drawing.Point(10, 74);
            this.rboMultiDate.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.rboMultiDate.Name = "rboMultiDate";
            this.rboMultiDate.Size = new System.Drawing.Size(320, 28);
            this.rboMultiDate.TabIndex = 1;
            this.rboMultiDate.TabStop = true;
            this.rboMultiDate.Text = "Выбор  временного промежутка";
            this.rboMultiDate.UseVisualStyleBackColor = true;
            this.rboMultiDate.CheckedChanged += new System.EventHandler(this.rboMultiDate_CheckedChanged);
            // 
            // dtpAdditionalDate
            // 
            this.dtpAdditionalDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAdditionalDate.Location = new System.Drawing.Point(132, 211);
            this.dtpAdditionalDate.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.dtpAdditionalDate.Name = "dtpAdditionalDate";
            this.dtpAdditionalDate.Size = new System.Drawing.Size(159, 29);
            this.dtpAdditionalDate.TabIndex = 7;
            // 
            // dtpMainDate
            // 
            this.dtpMainDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpMainDate.Location = new System.Drawing.Point(132, 171);
            this.dtpMainDate.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.dtpMainDate.Name = "dtpMainDate";
            this.dtpMainDate.Size = new System.Drawing.Size(159, 29);
            this.dtpMainDate.TabIndex = 6;
            // 
            // lblAdditionalDate
            // 
            this.lblAdditionalDate.AutoSize = true;
            this.lblAdditionalDate.Location = new System.Drawing.Point(7, 215);
            this.lblAdditionalDate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblAdditionalDate.Name = "lblAdditionalDate";
            this.lblAdditionalDate.Size = new System.Drawing.Size(115, 24);
            this.lblAdditionalDate.TabIndex = 3;
            this.lblAdditionalDate.Text = "Заканчивая";
            // 
            // lblMainDate
            // 
            this.lblMainDate.AutoSize = true;
            this.lblMainDate.Location = new System.Drawing.Point(10, 175);
            this.lblMainDate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblMainDate.Name = "lblMainDate";
            this.lblMainDate.Size = new System.Drawing.Size(100, 24);
            this.lblMainDate.TabIndex = 2;
            this.lblMainDate.Text = "Начиная с";
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(706, 15);
            this.btnSelect.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(137, 42);
            this.btnSelect.TabIndex = 3;
            this.btnSelect.Text = "Выбрать";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(706, 69);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(137, 42);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmDataSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 399);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.grpSelectionOfDate);
            this.Controls.Add(this.grpSelectionOfTextFields);
            this.Controls.Add(this.grpFieldForSelection);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "frmDataSelection";
            this.Text = "Выбор даных";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDataSelection_FormClosing);
            this.grpFieldForSelection.ResumeLayout(false);
            this.grpFieldForSelection.PerformLayout();
            this.grpSelectionOfTextFields.ResumeLayout(false);
            this.grpSelectionOfTextFields.PerformLayout();
            this.grpSelectionOfDate.ResumeLayout(false);
            this.grpSelectionOfDate.PerformLayout();
            this.grpSelectionOfFormatData.ResumeLayout(false);
            this.grpSelectionOfFormatData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpFieldForSelection;
        private System.Windows.Forms.CheckBox chkSecondName;
        private System.Windows.Forms.CheckBox chkDate;
        private System.Windows.Forms.CheckBox ckhCountry;
        private System.Windows.Forms.CheckBox chkCity;
        private System.Windows.Forms.CheckBox chkSurName;
        private System.Windows.Forms.CheckBox chkFirstName;
        private System.Windows.Forms.GroupBox grpSelectionOfTextFields;
        private System.Windows.Forms.TextBox txtCountry;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox txtSurName;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtSecondName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpSelectionOfDate;
        private System.Windows.Forms.GroupBox grpSelectionOfFormatData;
        private System.Windows.Forms.RadioButton rboSingleDate;
        private System.Windows.Forms.RadioButton rboMultiDate;
        private System.Windows.Forms.DateTimePicker dtpAdditionalDate;
        private System.Windows.Forms.DateTimePicker dtpMainDate;
        private System.Windows.Forms.Label lblAdditionalDate;
        private System.Windows.Forms.Label lblMainDate;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnCancel;
    }
}