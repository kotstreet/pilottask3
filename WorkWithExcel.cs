﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using PilotTask3;
using ExcelApplication = Microsoft.Office.Interop.Excel.Application;


public class WorkWithExcel
{
    //private static int _numberOfFile = 0;///////////////////////////

    private  ExcelApplication _excelApplication;
    private  Workbook _workbook;
    private  Worksheet _worksheet;

    /// <summary>
    /// write data inte new excel file
    /// </summary>
    /// <param name="people">list of people who will haved to write into excel</param>
    public async void Execute(List<Person> people, string pathForFile)
    {
        await Task.Factory.StartNew(() =>
        {
        //create excele file
        CreateExcelFileAndGetWorksheet(pathForFile);

        //add header
        AddHeader();

        //set width for column in the sheet
        _excelApplication.Columns.ColumnWidth = Constants.WidthForColumns;

        //fill other cells
        FillData(people);

        //show excel
        _excelApplication.Visible = true;

        });
    }

    /// <summary>
    /// open excel file, create workbook and get active worksheet
    /// </summary>
    private void CreateExcelFileAndGetWorksheet(string pathForFile)
    {
        //open excel
        _excelApplication = new ExcelApplication();

        //check for file exists and create file if it needed
        if(File.Exists(pathForFile) == false)
        {
            File.Create(pathForFile).Close();
        }

        //open workbook 
        _workbook = _excelApplication.Workbooks.Open(pathForFile);

        //get active sheet
        _worksheet = _workbook.ActiveSheet as Worksheet;
    }

    /// <summary>
    /// Add header in excel worksheet
    /// </summary>
    private void AddHeader()
    {
        //setting font for header
        _worksheet.Rows[Constants.RowForHeaderInExcel].Font.Bold = true;
        _worksheet.Rows[Constants.RowForHeaderInExcel].Font.Color = Color.BlueViolet;
        _worksheet.Rows[Constants.RowForHeaderInExcel].Font.Size += Constants.IncreaceForFontSize;

        //fill the header
        _worksheet.Range["A1","F1"].Value = new string[] { Constants.HeaderTextForSecondName, Constants.HeaderTextForFirstName, 
            Constants.HeaderTextForSurName, Constants.HeaderTextForCity, Constants.HeaderTextForCountry, Constants.HeaderTextForDate};
    }

    /// <summary>
    /// fill fileds in excel with data from people
    /// </summary>
    private void FillData(List<Person> people)
    {
        int addIndex = Constants.AddingIndexForToStartFillingData;

        //cycle start from 0, but the row starts from 2
        for (int indexOfElement = 0; indexOfElement < people.Count; indexOfElement++)
        {
            //get current person for geting fields values
            Person currentPerson = people.ElementAt(indexOfElement);

            //fill all cells in the current row
            string[] stringsForWorkSheet = new string[] { currentPerson.LastName, currentPerson.FirstName, currentPerson.SurName,
                currentPerson.City, currentPerson.Country, currentPerson.Date.ToShortDateString()};
            _worksheet.Range[$"F{indexOfElement + addIndex}", $"A{indexOfElement + addIndex}"].Value = stringsForWorkSheet;
        }

    }
}
